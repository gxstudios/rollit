﻿using UnityEngine;
using System.Collections;

public class respawn : MonoBehaviour {
	public float threshold;
	public Vector3 checkpoint;
	void Update() {
		/* This is the script that allows you to set a checkpoint. */
		if (Input.GetKeyDown ("c")) {
			checkpoint = GameObject.Find("Player").transform.position;

		}
		// This script tells the computer this:
		/*
		 * If key 'R' has been presed then
		 * set the variable transform.position to a new object of type Vector3 and set its x, y, z valuses to 0, 0, 0
		 * then change the current value of checkpoint to a new Vector3 with x, y, z 0, 0, 0
		 */
		if (Input.GetKeyDown ("r")) {
			transform.position = new Vector3 (0, 0, 0);
			checkpoint = new Vector3 (0, 0, 0);
		}
	
	}
	void FixedUpdate () {
		if (transform.position.y < threshold)
			transform.position = checkpoint;
	}
}
/* This is my games respawn script. It detects when the player oes below a certan point and then put's it back to the last set checkpoint. */
